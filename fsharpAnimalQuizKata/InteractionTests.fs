namespace animalquizInteractionTests

module tests =

    open System
    open NUnit.Framework
    open fsharpAnimalQuizKata.RunModule
    open System.Collections
    open System.Collections.Generic
    open fsharpAnimalQuizKata.IoModule
    open fsharpAnimalQuizKata.BrainModule
    open Rhino.Mocks
    open fsharpAnimalQuizKata.RecordUtils

    [<Test>]
        let first_test() =
            Assert.IsTrue(true)

    [<Test>]
    let ``in the initial welcome state, should invite to think about an animal``() =
        // setup
        let mockrepo = new MockRepository()
        let inStream = mockrepo.DynamicMock<InStream>()
        let outStream = mockrepo.StrictMock<OutStream>()

        // expectation
        Expect.Call(outStream.Write("think about an animal")) |> ignore
        mockrepo.ReplayAll()

        // arrange
        let tree = AnimalName "elephant"
        let initialState = Welcome
        let numLoops = 1

        // act
        runUntilIndex outStream inStream tree initialState numLoops |> ignore

        // verify expectations
        mockrepo.VerifyAll()
                  
    [<Test>]
    let ``in the state InviteToThinkAboutAnAnimal, given a only one leaf node root should guess that it is the animal in the root``() =
        // setup
        let mockrepo = new MockRepository()
        let inStream = mockrepo.DynamicMock<InStream>()
        let outStream = mockrepo.StrictMock<OutStream>()

        // expectations
        Expect.Call(outStream.Write("is it a cat?")) |> ignore
        mockrepo.ReplayAll()

        // arrange
        let tree = AnimalName "cat"
        let initialState = InviteToThinkAboutAnAnimal
        let numLoops = 1

        // act
        runUntilIndex outStream inStream tree InviteToThinkAboutAnAnimal numLoops |> ignore

        // verify expectations
        mockrepo.VerifyAll()




[<Test>]
    let ``with a one leaf node knowledge tree, engine asks if it is the animal on that node, the user says yes, and so the engine answer with "yeah!"``() =
        // setup 
        let mockrepo = new MockRepository()
        let inStream = mockrepo.StrictMock<InStream>()

        let outStream = mockrepo.StrictMock<OutStream>()

        // setup expectations
        Expect.Call(outStream.Write("think about an animal")) |> ignore
        Expect.Call(inStream.Input()).Return("whatever") |> ignore

        Expect.Call(outStream.Write("is it a cat?")) |> ignore     
        Expect.Call(inStream.Input()).Return("yes") |> ignore

        Expect.Call(outStream.Write("yeah!")) |> ignore 
        Expect.Call(inStream.Input()).Return("anything") |> ignore
          
        mockrepo.ReplayAll()

        // arrange
        let tree = AnimalName "cat"
        let initState = Welcome
        let numOfLoops = 3

        // act
        runUntilIndex outStream inStream tree initState numOfLoops |> ignore

        // verify expectations
        mockrepo.VerifyAll()


    [<Test>]
    let ``given one node knowledge tree, the engine asks if it is such animal, and when the user says no, then the engine asks "what animal was"``() =
        // setup stubs
        let mockrepo = new MockRepository()
        let inStream = mockrepo.StrictMock<InStream>()
        let outStream = mockrepo.StrictMock<OutStream>()

        // mocking
        Expect.Call(outStream.Write("think about an animal")) |> ignore
        Expect.Call(inStream.Input()).Return("anything") |> ignore

        Expect.Call(outStream.Write("is it a cat?")) |> ignore     
        Expect.Call(inStream.Input()).Return("no") |> ignore

        Expect.Call(outStream.Write("what animal was?")) |> ignore     
        Expect.Call(inStream.Input()).Return("any") |> ignore
          
        mockrepo.ReplayAll()
          
        // arrange
        let tree = AnimalName "cat"
        let initState = Welcome

        // act
        runUntilIndex outStream inStream tree initState 3 |> ignore

        // assert
        mockrepo.VerifyAll()


    [<Test>]
    let ``the system will ask if is the animal on the root, and the use says no, and so the system will ask a question do distinguish them``() =
        // setup stubs
        let mockrepo = new MockRepository()
        let inStream = mockrepo.StrictMock<InStream>()
        let outStream = mockrepo.StrictMock<OutStream>()

        Expect.Call(outStream.Write("think about an animal")) |> ignore
        Expect.Call(inStream.Input()).Return("") |> ignore

        Expect.Call(outStream.Write("is it a dog?")) |> ignore
        Expect.Call(inStream.Input()).Return("no") |> ignore

        Expect.Call(outStream.Write("what animal was?")) |> ignore
        Expect.Call(inStream.Input()).Return("cat") |> ignore

        Expect.Call(outStream.Write("please, write a yes/no question to distinguish a cat from a dog")) |> ignore
        Expect.Call(inStream.Input()).Return("does it bark?") |> ignore

        Expect.Call(outStream.Write("what is the answer to the question \"does it bark?\" to distinguish a cat from a dog?")) |> ignore
        Expect.Call(inStream.Input()).Return("no") |> ignore
          
        mockrepo.ReplayAll()

        // arrange
        let tree = AnimalName "dog"

        // act
        runUntilIndex outStream inStream tree Welcome 5 |> ignore

        // assert
        mockrepo.VerifyAll()



    [<Test>]
    let ``update the knowledge tree``() =
        // setup
        let mockrepo = new MockRepository()
        let inStream = mockrepo.StrictMock<InStream>()
        let outStream = mockrepo.StrictMock<OutStream>()


        // mocking
        Expect.Call(outStream.Write("think about an animal")) |> ignore
        Expect.Call(inStream.Input()).Return("") |> ignore

        Expect.Call(outStream.Write("is it a dog?")) |> ignore
        Expect.Call(inStream.Input()).Return("no") |> ignore

        Expect.Call(outStream.Write("what animal was?")) |> ignore
        Expect.Call(inStream.Input()).Return("cat") |> ignore

        Expect.Call(outStream.Write("please, write a yes/no question to distinguish a cat from a dog")) |> ignore
        Expect.Call(inStream.Input()).Return("does it bark?") |> ignore

        Expect.Call(outStream.Write("what is the answer to the question \"does it bark?\" to distinguish a cat from a dog?")) |> ignore
        Expect.Call(inStream.Input()).Return("no") |> ignore

        Expect.Call(outStream.Write("ok")) |> ignore
        Expect.Call(inStream.Input()).Return("") |> ignore

        Expect.Call(outStream.Write("think about an animal")) |> ignore
        Expect.Call(inStream.Input()).Return("") |> ignore

        Expect.Call(outStream.Write("does it bark?")) |> ignore
        Expect.Call(inStream.Input()).Return("no") |> ignore

        mockrepo.ReplayAll()


        // arrange
        let tree = AnimalName "dog"
        let initState = Welcome

        // act
        runUntilIndex outStream inStream tree initState 8 |> ignore

        // assert
        mockrepo.VerifyAll()



















    
